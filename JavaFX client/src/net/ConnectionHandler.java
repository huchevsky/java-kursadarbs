package net;

import client.controller.ScreenController;
import javafx.concurrent.Task;
import net.packet.Packet;
import net.packet.PacketSender;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by huch on 23.05.2016..
 * Handles all networking stuff with server.
 */
public class ConnectionHandler {

    /**
     * Servers socket.
     */
    private Socket socket;

    /**
     * PacketSender is a class, which deals with packet sending to server.
     */
    private PacketSender packetSender;

    /**
     * PacketListener is a class, which deals with packet receiving from server.
     */
    private PacketListener packetListener;

    private ScreenController screenController;

    /**
     * Creates ConnectionListener which is connected with Server.
     * @param ip
     * @param port
     */
    public ConnectionHandler(String ip, int port, ScreenController screenController) {
        this.screenController = screenController;
        try {
            socket = new Socket(ip, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            packetSender = new PacketSender(new ObjectOutputStream(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            packetListener = new PacketListener(new ObjectInputStream(socket.getInputStream()), screenController);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                while(true) {
                    handle();
                }
            }

        };
       new Thread(task).start();
    }

    public void handle() {
            try {
                Object obj = getPacketListener().getInputStream().readObject();
                getPacketListener().handlePackets((Packet)obj);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
    }



    public PacketListener getPacketListener() {
        return packetListener;
    }

    public PacketSender getPacketSender() {
        return packetSender;
    }

}
