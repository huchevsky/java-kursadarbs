package net.packet;

import java.io.Serializable;

/**
 * Created by huch on 05.06.2016..
 */
public abstract class Packet implements Serializable {

    public abstract int getPacketID();

    public abstract int getPacketGroupID();

}
