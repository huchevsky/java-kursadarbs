package net.packet.client;

import net.packet.Packet;

import java.io.Serializable;

/**
 * Created by huch on 05.06.2016..
 */
public class Login extends Packet implements Serializable {

    private String username;
    private String password;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public int getPacketID() {
        return 1;
    }

    @Override
    public int getPacketGroupID() {
        return 0;
    }
}
