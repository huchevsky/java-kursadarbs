package net.packet.client;

import net.packet.Packet;

import java.io.Serializable;

/**
 * Created by huch on 13.06.2016..
 */
public class Shoot extends Packet implements Serializable {

    private String shotOwner;

    public Shoot(String shotOwner) {
        this.shotOwner = shotOwner;
    }

    public String getShotOwner() {
        return shotOwner;
    }

    @Override
    public int getPacketID() {
        return 1;
    }

    @Override
    public int getPacketGroupID() {
        return 2;
    }
}
