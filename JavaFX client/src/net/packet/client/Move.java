package net.packet.client;

import net.packet.Packet;

import java.io.Serializable;

/**
 * Created by huch on 13.06.2016..
 */
public class Move extends Packet implements Serializable {

    private String moveDir;

    public Move(String moveDir) {
        this.moveDir = moveDir;
    }

    public String getMoveDir() {
        return moveDir;
    }

    @Override
    public int getPacketID() {
        return 0;
    }

    @Override
    public int getPacketGroupID() {
        return 0;
    }
}
