package net.packet.client;

import net.packet.Packet;

import java.io.Serializable;

/**
 * Created by huch on 12.06.2016..
 */
public class Challenge extends Packet implements Serializable {

    private String challengedUser;

    public Challenge(String challengedUser) {
        this.challengedUser = challengedUser;
    }

    public String getChallengedUser() {
        return challengedUser;
    }

    @Override
    public int getPacketID() {
        return 1;
    }

    @Override
    public int getPacketGroupID() {
        return 1;
    }
}
