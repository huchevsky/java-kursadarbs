package net.packet.server;

import net.packet.Packet;

import java.io.Serializable;
import java.util.List;

/**
 * Created by huch on 11.06.2016..
 */
public class LobbyList extends Packet implements Serializable {

    private List<String> playersInLobby;

    public LobbyList(List<String> playersInLobby) {
        this.playersInLobby = playersInLobby;
    }

    public List<String> getPlayersInLobby() {
        return playersInLobby;
    }

    @Override
    public int getPacketID() {
        return 0;
    }

    @Override
    public int getPacketGroupID() {
        return 1;
    }
}
