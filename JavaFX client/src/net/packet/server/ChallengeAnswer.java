package net.packet.server;

import net.packet.Packet;

import java.io.Serializable;

/**
 * Created by huch on 12.06.2016..
 */
public class ChallengeAnswer extends Packet implements Serializable {

    private String challenger;

    public ChallengeAnswer(String challenger) {
        this.challenger = challenger;
    }

    public String getChallenger() {
        return challenger;
    }

    @Override
    public int getPacketID() {
        return 2;
    }

    @Override
    public int getPacketGroupID() {
        return 1;
    }
}

