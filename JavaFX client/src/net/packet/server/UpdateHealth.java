package net.packet.server;

import net.packet.Packet;

import java.io.Serializable;

/**
 * Created by huch on 13.06.2016..
 */
public class UpdateHealth extends Packet implements Serializable {

    private double player1Health;
    private double player2Health;
    private String player1Name;
    private String player2Name;

    public UpdateHealth(double player1Health, double player2Health, String player1Name, String player2Name) {
        this.player1Health = player1Health;
        this.player2Health = player2Health;
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public double getPlayer1Health() {
        return player1Health;
    }

    public double getPlayer2Health() {
        return player2Health;
    }

    public String getPlayer1Name() {
        return player1Name;
    }

    public String getPlayer2Name() {
        return player2Name;
    }

    @Override
    public int getPacketID() {
        return 0;
    }

    @Override
    public int getPacketGroupID() {
        return 0;
    }
}
