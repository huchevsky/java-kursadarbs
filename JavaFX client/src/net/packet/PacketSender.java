package net.packet;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by huch on 05.06.2016..
 * PacketSender is a class, which handles all packet sending.
 */
public class PacketSender {

    /**
     * Socket which is connected with server.
     */
    private ObjectOutputStream out;

    public PacketSender(ObjectOutputStream out) {
        this.out = out;
    }

    public boolean sendPacket(Packet packet) {
        if(packet != null || out != null) {
            try {
                out.writeObject(packet);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }
}
