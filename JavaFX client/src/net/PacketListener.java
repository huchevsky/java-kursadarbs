package net;

import client.controller.GameController;
import client.controller.LobbyController;
import client.controller.ScreenController;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import net.packet.Packet;
import net.packet.server.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Optional;

/**
 * Created by huch on 05.06.2016..
 * PacketSender is class, which handles all incoming packets from server.
 */
public class PacketListener {

    /**
     * Socket which is connected with server.
     */
    private ObjectInputStream in;

    private ScreenController screenController;

    public PacketListener(ObjectInputStream in, ScreenController screenController) {
       this.in = in;
        this.screenController = screenController;
    }

    public  void handlePackets(Packet packet) throws IOException, ClassNotFoundException {
        if(packet instanceof Answer) {
            Answer answer = (Answer)packet;
            if(answer.getAnswerId() == 1) {
                Platform.runLater(() -> screenController.setScreen("lobby"));
            } else if(answer.getAnswerId() == 73) {
                Platform.runLater(() -> screenController.setScreen("game"));
                GameController lo =  (GameController) screenController.getScreens().get("game");
                lo.startGame();
            }
        }

        if(packet instanceof LobbyList) {
            LobbyList lobby = (LobbyList)packet;
            LobbyController lo =  (LobbyController)screenController.getScreens().get("lobby");
            lo.setList(lobby.getPlayersInLobby());
        }

        if(packet instanceof UpdateHealth) {
            UpdateHealth updateHealth = (UpdateHealth) packet;
            GameController lo =  (GameController) screenController.getScreens().get("game");
            Platform.runLater(() ->lo.setInfo(updateHealth.getPlayer1Health(), updateHealth.getPlayer2Health(), updateHealth.getPlayer1Name(), updateHealth.getPlayer2Name()));
        }

        if(packet instanceof UpdateEntities) {
            System.out.println("lol, happens");
            UpdateEntities updateEntities = (UpdateEntities) packet;
            GameController lo =  (GameController) screenController.getScreens().get("game");
            Platform.runLater(() -> lo.setEntities(updateEntities.getPlayer1(), updateEntities.getPlayer2()));
        }

        if(packet instanceof ChallengeAnswer) {
            ChallengeAnswer challengeAnswer = (ChallengeAnswer)packet;

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Are you ready to fight!");
                    alert.setHeaderText("User " + challengeAnswer.getChallenger() + " just challanged you!");
                    alert.setContentText("Do you accept the fight?");
                    Optional<ButtonType> result = alert.showAndWait();
                    if(result.get()==ButtonType.OK) {
                        // ... user chose OK
                        screenController.getConnection().getPacketSender().sendPacket(new Answer((byte)69));
//                        screenController.setScreen("game");
//                        GameController lo =  (GameController) screenController.getScreens().get("game");
//                        lo.startGame();
                    } else {
                        // ... user chose CANCEL or closed the dialog
                        System.out.println("nope");
                    }
                }
            });
        }


    }

    public ObjectInputStream getInputStream() {
        return in;
    }
}
