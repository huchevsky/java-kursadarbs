package client;

import client.controller.ScreenController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        ScreenController controller = new ScreenController(primaryStage);
        controller.addScreen("login", Constants.LOGIN_SCREEN);
        controller.addScreen("registration", Constants.REG_SCREEN);
        controller.addScreen("lobby", Constants.LOBBY_SCREEN);
        controller.addScreen("game", Constants.GAME_SCREEN);
        primaryStage.setScene(new Scene(controller.getParent()));
        controller.setScreen("login");
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
