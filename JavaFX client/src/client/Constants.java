package client;

/**
 * Created by huch on 23.05.2016..
 */
public class Constants {

        public static final String LOGIN_SCREEN = "fxml/Login.fxml";
        public static final String REG_SCREEN = "fxml/Registration.fxml";
        public static final String LOBBY_SCREEN = "fxml/Lobby.fxml";
        public static final String GAME_SCREEN = "fxml/Game.fxml";
}
