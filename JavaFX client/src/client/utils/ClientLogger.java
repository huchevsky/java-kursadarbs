package client.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by huch on 20.05.2016..
 */
public class ClientLogger {
    private final static Logger LOGGER = Logger.getLogger("Client");

    public static void logInfo(Class clazz, String msg) {
        LOGGER.log(Level.INFO,"(" + clazz.getName() + ") " + msg);
    }

    public static void logError(Class clazz, String msg) {
        LOGGER.log(Level.WARNING,"(" + clazz.getName() + ") " + msg);
    }

}
