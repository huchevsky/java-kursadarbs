package client.controller;

import client.Main;
import client.utils.ClientLogger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import net.ConnectionHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by huch on 22.05.2016..
 */
public class ScreenController {

    private Map<String, Screen> screens = new HashMap<String, Screen>();

    private ConnectionHandler connection;

    private Stage primaryStage;

    private Parent root;

    public ScreenController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.connection = new ConnectionHandler("localhost", 1339, this);
    }

    public boolean addScreen(String screenName, String screenPath) {
        if(screens.containsKey(screenName)) {
            ClientLogger.logError(this.getClass(), "Screen ("  + screenName + ") has already been loaded!");
            return false;
        } else {
            try {
                FXMLLoader myLoader = new FXMLLoader(Main.class.getResource(screenPath));
                Parent parent = myLoader.load();
                Screen screen = myLoader.getController();
                screen.setScreenNode(parent);
                screen.setScreenController(this);
                screens.put(screenName, screen);
                if(root == null) {
                    root = parent;
                }
                ClientLogger.logInfo(this.getClass(), "Screen (" + screenName + ") has loaded!");
                return true;
            } catch (IOException e) {
                ClientLogger.logInfo(this.getClass(), "Can't find path for screen " + screenName + "!");
            }
        }
        return false;
    }

    public void setScreen(String screenName) {
        Screen screen = screens.get(screenName);
        if(screen == null) {
            ClientLogger.logError(this.getClass(), "There is no screen with name ("  + screenName + ")!");
            return;
        }
        root = (Parent)screens.get(screenName).getScreenNode();
        primaryStage.setWidth(screen.getWidth());
        primaryStage.setHeight(screen.getHeight());
        primaryStage.setTitle(screen.getScreenTitle());
        primaryStage.getScene().setRoot(root);
        primaryStage.show();
        ClientLogger.logInfo(this.getClass(), "Screen with name ("  + screenName + ") set as main screen!");
    }

    public Map<String, Screen> getScreens() {
        return screens;
    }

    public Parent getParent() {
        return root;
    }

    public ConnectionHandler getConnection() {
        return connection;
    }
}
