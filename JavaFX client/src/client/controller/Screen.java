package client.controller;

import javafx.scene.Node;

/**
 * Created by huch on 23.05.2016..
 */
public interface Screen {

    int getWidth();

    int getHeight();

    String getScreenName();

    String getScreenTitle();

    void setScreenController(ScreenController controller);

    void setScreenNode(Node node);

    Node getScreenNode();
}
