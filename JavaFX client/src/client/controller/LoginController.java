package client.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import net.packet.client.Login;

public class LoginController implements Screen {

    @FXML
    private Button login;

    @FXML
    private Button register;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    private ScreenController screenController;

    private Node screenNode;

    @FXML
    protected void loginButton(ActionEvent event) {
        screenController.getConnection().getPacketSender().sendPacket(new Login(username.getText(), password.getText()));
    }

    @FXML
    protected void registerButton(ActionEvent event) {
        screenController.setScreen("registration");
    }

    @Override
    public int getWidth() {
        return 500;
    }

    @Override
    public int getHeight() {
        return 350;
    }

    @Override
    public String getScreenName() {
        return "login";
    }

    @Override
    public String getScreenTitle() {
        return "Login Screen";
    }

    @Override
    public void setScreenController(ScreenController controller) {
        screenController = controller;
    }

    @Override
    public void setScreenNode(Node node) {
        screenNode = node;
    }

    @Override
    public Node getScreenNode() {
        return screenNode;
    }

}
