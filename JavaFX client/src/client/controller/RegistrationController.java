package client.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import net.packet.client.Register;

public class RegistrationController implements Screen {

    @FXML
    public TextField username;

    @FXML
    public PasswordField password;

    @FXML
    public PasswordField passwordAgain;

    @FXML
    public Button register;

    @FXML
    public Button back;

    private ScreenController screenController;

    private Node screenNode;

    @FXML
    protected void register(ActionEvent event) {
        if(!username.getText().isEmpty() && !password.getText().isEmpty() && !passwordAgain.getText().isEmpty()) {
            if(password.getText().equals(passwordAgain.getText())) {
                //send register packet
                screenController.getConnection().getPacketSender().sendPacket(new Register(username.getText(), password.getText()));
            }
        }
    }

    @FXML
    protected void backAction(ActionEvent event) {
        screenController.setScreen("login");
    }

    @Override
    public int getWidth() {
        return 500;
    }

    @Override
    public int getHeight() {
        return 360;
    }

    @Override
    public String getScreenName() {
        return "registration";
    }

    @Override
    public String getScreenTitle() {
        return "Registration Screen";
    }

    @Override
    public void setScreenController(ScreenController controller) {
        screenController = controller;
    }

    @Override
    public void setScreenNode(Node node) {
        screenNode = node;
    }

    @Override
    public Node getScreenNode() {
        return screenNode;
    }
}
