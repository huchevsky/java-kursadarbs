package client.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.packet.client.Challenge;

import java.util.List;
import java.util.Optional;

/**
 * Created by huch on 08.06.2016..
 */
public class LobbyController implements Screen {

    @FXML
    private ListView<String> list;

    private ScreenController screenController;

    private Node screenNode;

    @Override
    public int getWidth() {
        return 500;
    }

    @Override
    public int getHeight() {
        return 400;
    }

    @Override
    public String getScreenName() {
        return "lobby";
    }

    @Override
    public String getScreenTitle() {
        return "Game Lobby";
    }

    private void showChallengeWait() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Waiting for response..");
        alert.setHeaderText("Waiting for user to answer.");
        alert.setContentText("Press cancel, to stop challenge.");

        ButtonType buttonTypeOne = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            // ... user chose "One"
            System.out.println("Cancel the challenge.");
        }
    }

    public void setList(List<String> entries) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(entries != null) {
                    list.getItems().clear();
                    list.getItems().addAll(FXCollections.observableList(entries));
                    list.refresh();
                    final ContextMenu cm = new ContextMenu();
                    MenuItem menuItem = new MenuItem("Challenge");
                    cm.getItems().addAll(menuItem);
                    menuItem.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            //send challange packet
                            String user = list.getSelectionModel().getSelectedItem();
                            screenController.getConnection().getPacketSender().sendPacket(new Challenge(user));
                            Platform.runLater(() -> showChallengeWait());
                        }
                    });
                    list.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            if (event.getButton().equals(MouseButton.SECONDARY)) {
                                cm.show(list, event.getScreenX(), event.getScreenY());
                            } else {
                                cm.hide();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void setScreenController(ScreenController controller) {
        screenController = controller;
    }

    @Override
    public void setScreenNode(Node node) {
        screenNode = node;
    }

    @Override
    public Node getScreenNode() {
        return screenNode;
    }
}
