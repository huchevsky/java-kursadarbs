package client.controller;

import client.Main;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.TimelineBuilder;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import net.Entity;
import net.packet.client.Move;
import net.packet.client.Shoot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huch on 12.06.2016..
 */
public class GameController implements Screen {

    private ScreenController screenController;

    private Node screenNode;

    @FXML
    private Canvas gameCanvas;

    @FXML
    private ProgressBar healthEntity1;

    @FXML
    private ProgressBar healthEntity2;

    @FXML
    private Label nameEntity1;

    @FXML
    private Label nameEntity2;


    private Entity player1 = null;
    private Entity player2 = null;
    private List<Entity> bullets1 = null;
    private List<Entity> bullets2 = null;



    //private Image image = new Image("resources/ship.png");
    Image cap = new Image(Main.class.getResource("fxml/resources/ship.png")+"");

    public void setInfo(double entityHealth1, double entityHealth2, String nameEntity1, String nameEntity2) {
        System.out.println(entityHealth1 + " " + (entityHealth1 / 100));
        healthEntity1.setProgress(entityHealth1 / 100);
        healthEntity2.setProgress(entityHealth2 / 100);
        this.nameEntity1.setText(nameEntity1);
        this.nameEntity2.setText(nameEntity2);
    }

    public void setEntities(Entity player1, Entity player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    private void draw( GraphicsContext gc) {
       // gc.setFill( new Color(0.85, 0.85, 1.0, 1.0) );
        gc.setFill(Color.LIGHTGRAY);
        gc.fillRect(0, 0, 300, 300);
        if(player1 != null) {
            gc.drawImage(cap, player1.getX(), player1.getY());
        }
        if(player2 != null) {
            gc.drawImage(cap, player2.getX(), player2.getY());
        }
    }

    private void input() {
        gameCanvas.setFocusTraversable(true);
        ArrayList<String> input = new ArrayList<String>();
        System.out.println("happens");
        screenNode.setOnKeyPressed(
                new EventHandler<KeyEvent>()
                {
                    @Override
                    public void handle(KeyEvent e)
                    {
                        String code = e.getCode().toString();
                        System.out.println(code);
                        if(code.equals("A") || code.equals("D")) {
                            screenController.getConnection().getPacketSender().sendPacket(new Move(code));
                        } else if(code.equals("SPACE")) {
                           // screenController.getConnection().getPacketSender().sendPacket(new Shoot(code));
                        }
                        // only add once... prevent duplicates
                        if ( !input.contains(code) )
                            input.add( code );
                    }
                });

        screenNode.setOnKeyReleased(
                new EventHandler<KeyEvent>()
                {
                    @Override
                    public void handle(KeyEvent e)
                    {
                        String code = e.getCode().toString();
                        input.remove( code );
                    }
                });

    }

    public void startGame() {

        final Duration oneFrameAmt = Duration.millis(1000/60);
        GraphicsContext gc = gameCanvas.getGraphicsContext2D();
        input();

        gameCanvas.setStyle("-fx-effect: innershadow(gaussian, #039ed3, 1, 1.0, 0, 0);");

        final KeyFrame oneFrame = new KeyFrame(oneFrameAmt,
                event -> {
                    draw(gc);
                    // update actors
                    // updateSprites();

                    // check for collision
                    //  checkCollisions();

                    // removed dead things
                    //cleanupSprites();
                }); // oneFrame

                // sets the game world's game loop (Timeline)
                 TimelineBuilder.create()
                .cycleCount(Animation.INDEFINITE)
                .keyFrames(oneFrame)
                .build()
                .play();
    }

    @Override
    public int getWidth() {
        return 300;
    }

    @Override
    public int getHeight() {
        return 300;
    }

    @Override
    public String getScreenName() {
        return "game";
    }

    @Override
    public String getScreenTitle() {
        return "Game";
    }

    @Override
    public void setScreenController(ScreenController controller) {
        this.screenController = controller;
    }

    @Override
    public void setScreenNode(Node node) {
        this.screenNode = node;
    }

    @Override
    public Node getScreenNode() {
        return screenNode;
    }
}
