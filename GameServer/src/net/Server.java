package net;

import server.Constants;
import server.Database;
import server.utils.ServerLogger;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by huch on 20.05.2016..
 * Main server thread handler.
 */
public class Server implements Runnable {

    /**
     * Server's name.
     */
    private String serverName;

    /**
     * Port which is used for server host.
     */
    private int serverPort;

    /**
     * Contains information, if server is running.
     */
    private boolean serverRunning = false;

    /**
     * Server socket.
     */
    private ServerSocket serverSocket;

    private ConnectionListener connectionListener;

    private Database db;

    /**
     * Creates new server with custom server name and port which server is listening to.
     * @param serverName
     * @param serverPort
     */
    public Server(String serverName, int serverPort) {
        this.serverName = serverName;
        this.serverPort = serverPort;
        db = new Database();
        initServer();
    }

    /**
     * Initializes everything for server before it is running.
     */
    private void initServer() {
        serverRunning = true;
        createServerSocket();
        createConnectionListener(serverSocket, Constants.MAX_USERS);
        run();
    }

    /**
     * Creates server on specific port, which it is listening to.
     */
    private void createServerSocket() {
        ServerLogger.logInfo(this.getClass(), "Starting server on port: " +serverPort + "!");
        try {
            serverSocket = new ServerSocket(serverPort);
        } catch (IOException e) {
            serverRunning = false;
            ServerLogger.logError(this.getClass(), "Failed to start server on port: " +serverPort + "!");
        }
    }

    /**
     * Creates server on specific port, which it is listening to.
     */
    private void createConnectionListener(ServerSocket serverSocket, int maxUsers) {
        ServerLogger.logInfo(this.getClass(), "Trying to start connection listener!");
        connectionListener = new ConnectionListener(serverSocket, db, maxUsers);
    }

    @Override
    public void run() {
        ServerLogger.logInfo(this.getClass(), "Server is now running!");
        while(serverRunning) {
            connectionListener.waitForConnections();
            connectionListener.handleConnections();
        }
        stop();
    }

    /**
     * Stops server and clears all resources.
     */
    public void stop() {
        ServerLogger.logInfo(this.getClass(), "Stopping server...");
        serverRunning = false;

        ServerLogger.logInfo(this.getClass(), "Saving player data and kicking out of server.");
        connectionListener.endConnections();
        ServerLogger.logInfo(this.getClass(), "Players saved and kicked out from server.");

        ServerLogger.logInfo(this.getClass(), "Closing server socket.");
        try {
            serverSocket.close();
        } catch (IOException e) {
            ServerLogger.logError(this.getClass(), "Can't close server socket.");
        }
        ServerLogger.logInfo(this.getClass(), "Server stopped!");
    }
}
