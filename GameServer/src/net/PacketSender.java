package net;

import net.packet.server.*;
import server.Database;
import server.game.lobby.Lobby;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by huch on 23.05.2016..
 */
public class PacketSender {

    private Database db;

    private Lobby lobby;

    private ConnectionListener connectionListener;

    public PacketSender(ConnectionListener connectionListener, Database db, Lobby lobby) {
        this.connectionListener = connectionListener;
        this.db = db;
        this.lobby = lobby;
    }

    public boolean sendAnswer(byte id, ObjectOutputStream out) {
        try {
            out.writeObject(new Answer(id));
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void sendLobbyList() {
        for(Client c : connectionListener.getConnections()) {
            if(c.getPlayer() != null) {
                if(lobby.inLobby(c.getPlayer().getUsername())) {
                    try {
                        System.out.println("User: " + c.getPlayer().getUsername() + " List: " + lobby.getPlayersInLobby());
                        c.getOutputStream().writeObject(new LobbyList(lobby.getPlayersInLobby()));
                        c.getOutputStream().flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public boolean sendUpdateHealth(UpdateHealth updateHealth, ObjectOutputStream out) {
        try {
            out.writeObject(updateHealth);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean sendEntities(UpdateEntities updateEntities, ObjectOutputStream out) {
        try {
            out.writeObject(updateEntities);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean sendChallengeAnswer(String challenger, ObjectOutputStream out) {
        try {
            out.writeObject(new ChallengeAnswer(challenger));
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
