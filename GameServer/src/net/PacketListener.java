package net;

import net.packet.Packet;
import net.packet.client.Challenge;
import net.packet.client.Login;
import net.packet.client.Move;
import net.packet.client.Register;
import net.packet.server.Answer;
import server.Database;
import server.game.entity.player.Player;
import server.utils.ServerLogger;

/**
 * Created by huch on 20.05.2016..
 */
public class PacketListener {

    private Database db;

    private ConnectionListener connectionListener;

    public PacketListener(ConnectionListener connectionListener, Database db) {
        this.db = db;
        this.connectionListener = connectionListener;
    }

    /**
     * Waits for client incoming packets and handles them by group.
     * @param packet incoming packet.
     */
    public void listenToPackets(Client player, Packet packet) {
        if(packet == null)
            return;
        if(packet instanceof Challenge) {
            Challenge ch = (Challenge)packet;
            if(player.getPlayer().getUsername().equals(ch.getChallengedUser())) {
                //return that you can't challenge yourself
            } else {
                for(Client c : connectionListener.getConnections()) {
                    if(c.getPlayer() != null) {
                        if (ch.getChallengedUser().equals(c.getPlayer().getUsername())) {
                            //send wait challenge answer
                            connectionListener.getChallengeController().addNewChallange(player, c);
                            connectionListener.getPacketSender().sendChallengeAnswer(player.getPlayer().getUsername(), c.getOutputStream());
                        }
                    }
                }

            }
        }

        if(packet instanceof Move) {
            Move move = (Move)packet;
            if(player.getGame().getPlayer().getPlayer().getUsername().equals(player.getPlayer().getUsername())) {
                player.getGame().setPosition1(move.getMoveDir());
            } else {
                player.getGame().setPosition2(move.getMoveDir());
            }
        }

        if(packet instanceof Answer) {
            Answer answer = (Answer)packet;
            if(answer.getAnswerId() == 69) {
                System.out.println("Received packet 69");
                if(player.getPlayer().getChallenge() != null) {
                    System.out.println("Sets true");
                    player.getPlayer().getChallenge().setPlayer2Status(true);
                }
            }
        }
    }

    /**
     * Handles received packet by it's id.
     * @param packet incoming packet to handle.
     */
    private void handlePacket(Packet packet) {
        switch(packet.getPacketID()) {
            default:
                System.out.println("Received packet");
        }
    }

    public Player handleLogin(Packet packet) {
        Login l = (Login) packet;
        ServerLogger.logInfo(this.getClass(), "Username = " + l.getUsername() + " password = " + l.getPassword());
        if(db.correctPassword(l.getUsername(), l.getPassword())) {
            return new Player(l.getUsername(), l.getPassword());
        } else {
            ServerLogger.logError(this.getClass(), "Can't handle login.");
            return null;
        }
    }

    public boolean handleRegister(Packet packet) {
        if(packet instanceof Register) {
            Register register = (Register)packet;
            if(!db.containsUser(register.getUsername())) {
                return db.addUserToDatabase(register.getUsername(), register.getPassword());
            }
            return false;
        }
        return false;
    }
}
