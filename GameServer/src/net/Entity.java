package net;

import java.io.Serializable;

/**
 * Created by huch on 08.06.2016..
 */
public class Entity implements Serializable {

    private double x;
    private double y;

    private double health;

    public Entity(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public double getHealth() {
        return health;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}

