package net;

import net.packet.Packet;
import net.packet.client.Login;
import net.packet.client.Register;
import server.game.Game;
import server.game.entity.player.Player;
import server.utils.ServerLogger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

/**
 * Created by huch on 20.05.2016..
 */
public class Client extends Thread {

    /**
     * Client socket.
     */
    private Socket socket;

    /**
     * Packet listener, which is handling all incoming packets from client.
     */
    private PacketListener packetListener;

    private PacketSender packetSender;

    /**
     * Socket's inputstream.
     */
    private ObjectInputStream in;

    /**
     * Socket's outputstream.
     */
    private ObjectOutputStream out;

    private Player player;

    private int id;

    private ConnectionListener connectionListener;

    private Game game;

    public Client(int id, Socket socket, PacketListener packetListener, PacketSender packetSender, ConnectionListener connectionListener) {
        this.id = id;
        this.socket = socket;
        player = null;
        this.packetListener = packetListener;
        this.packetSender = packetSender;
        this.connectionListener = connectionListener;
        try {
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            ServerLogger.logError(this.getClass(), "Can't get socket inputstream!");
        }

        try {
            out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            ServerLogger.logError(this.getClass(), "Can't get socket outputstream!");
        }
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    @Override
    public void run() {
        while(!socket.isClosed() && socket.isConnected()) {
            //do logic here
            Object obj = null;
            try {
                obj = in.readObject();
            } catch (IOException e) {
                closeConnection();
                ServerLogger.logError(this.getClass(), "Can't read data, ending connection!");
            } catch (ClassNotFoundException e) {
                ServerLogger.logError(this.getClass(), "Received wrong packet!");
                continue;
            }

            if(player != null) {
                packetListener.listenToPackets(this, (Packet)obj);
            } else {
                if (obj instanceof Login) {
                    player = packetListener.handleLogin((Packet) obj);
                    if(player != null) {
                        packetSender.sendAnswer((byte)1, out);
                        connectionListener.getLobby().joinLobby(player.getUsername());
                        packetSender.sendLobbyList();
                    } else {
                        packetSender.sendAnswer((byte)-1, out);
                    }
                } else if(obj instanceof Register) {
                    packetListener.handleRegister((Packet) obj);
                }

            }
        }
        closeConnection();
    }

    /**
     * Closes connection with client.
     */
    public void closeConnection() {
        //save user details before close
        if(player != null) {
            connectionListener.getLobby().removeFromLobby(player.getUsername());
        }
        List<Client> connections = connectionListener.getConnections();
        for(int i = 0; i < connections.size(); i++) {
            if(connections.get(i).getUniqueId() == id) {
                connections.remove(i);
            }
        }
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException e) {
            ServerLogger.logInfo(this.getClass(), "Client socket is closed!");
        }

    }

    public PacketSender getPacketSender() {
        return packetSender;
    }

    public int getUniqueId() {
        return id;
    }

    public Player getPlayer() {
        return player;
    }

    public ObjectOutputStream getOutputStream() {
        return out;
    }
}
