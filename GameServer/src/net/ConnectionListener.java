package net;

import server.Database;
import server.game.GameController;
import server.game.challenge.ChallengeController;
import server.game.lobby.Lobby;
import server.utils.ServerLogger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huch on 20.05.2016..
 */
public class ConnectionListener {

    /**
     * Server socket, which is used for listening for incoming connections.
     */
    private ServerSocket serverSocket = null;

    /**
     * Server user limit.
     */
    private int maxUsers = 10;

    /**
     * List of connected clients.
     */
    private List<Client> connections;

    /**
     * Client socket.
     */
    private Socket socket = null;

    private PacketListener packetListener;

    private PacketSender packetSender;

    private Database db;

    private int userId = 0;

    private Lobby lobby;

    private ChallengeController challengeController;

    public ConnectionListener(ServerSocket serverSocket, Database db, int maxUsers) {
        this.serverSocket = serverSocket;
        lobby = new Lobby();
        packetListener = new PacketListener(this, db);
        packetSender = new PacketSender(this, db, lobby);
        this.db = db;
        this.maxUsers = maxUsers;
        connections = new ArrayList<Client>(maxUsers);
        ServerLogger.logInfo(this.getClass(), "Connection listener created!");
        challengeController = new ChallengeController(lobby, new GameController(this));
    }

    /**
     * Waiting for next connection, if received adds it to connection list.
     */
    public void waitForConnections() {
        try {
            socket = serverSocket.accept();
            Client client = new Client(userId, socket, packetListener, packetSender, this);
            client.start();
            connections.add(client);
            ServerLogger.logInfo(this.getClass(), "New connection happened!");
            userId++;
        } catch (IOException e) {
            ServerLogger.logError(this.getClass(), "Something went wrong while someone tried to connect!");
        }
    }

    /**
     * Prints out how many players are at the moment online.
     */
    public void handleConnections() {
        ServerLogger.logInfo(this.getClass(), "Connections atm: " + connections.size());
    }

    /**
     * Closes all connections.
     * NOTE: Do this only if you are shutting out the server.
     */
    public void endConnections() {
        for(Client c : connections) {
            c.closeConnection();
        }
    }

    public ChallengeController getChallengeController() {
        return challengeController;
    }

    public List<Client> getConnections() {
        return connections;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public PacketSender getPacketSender() {
        return packetSender;
    }
}
