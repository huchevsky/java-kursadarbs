package net.packet;

/**
 * Created by huch on 20.05.2016..
 */
public abstract class Packet {

    public abstract int getPacketID();

    public abstract int getPacketGroupID();

}
