package net.packet.client;

import net.packet.Packet;

import java.io.Serializable;

/**
 * Created by huch on 09.06.2016..
 */
public class Register extends Packet implements Serializable {

    private String username;
    private String password;

    public Register(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }


    @Override
    public int getPacketID() {
        return 2;
    }

    @Override
    public int getPacketGroupID() {
        return 0;
    }
}

