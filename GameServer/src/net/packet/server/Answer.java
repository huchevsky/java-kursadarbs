package net.packet.server;

import net.packet.Packet;

import java.io.Serializable;

/**
 * Created by huch on 08.06.2016..
 */
public class Answer extends Packet implements Serializable {

    private byte answerId;

    public Answer(byte answerId) {
        this.answerId = answerId;
    }

    public byte getAnswerId() {
        return answerId;
    }

    @Override
    public int getPacketID() {
        return 0;
    }

    @Override
    public int getPacketGroupID() {
        return 0;
    }
}
