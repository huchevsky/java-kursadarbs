package net.packet.server;

import net.packet.Packet;
import net.Entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by huch on 12.06.2016..
 */
public class UpdateEntities extends Packet implements Serializable {

    private Entity player1;
    private Entity player2;
    private List<Entity> bullets;
    private List<Entity> bullets2;

    public UpdateEntities(Entity player1, Entity player2, List<Entity> bullets, List<Entity> bullets2) {
        this.player1 = player1;
        this.player2 = player2;
        this.bullets = bullets;
        this.bullets2 = bullets2;
    }

    public Entity getPlayer1() {
        return player1;
    }

    public Entity getPlayer2() {
        return player2;
    }

    public List<Entity> getBullets() {
        return bullets;
    }

    public List<Entity> getBullets2() {
        return bullets2;
    }

    @Override
    public int getPacketID() {
        return 0;
    }

    @Override
    public int getPacketGroupID() {
        return 0;
    }
}
