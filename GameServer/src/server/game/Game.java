package server.game;

import net.Client;
import net.ConnectionListener;
import net.Entity;
import net.packet.server.UpdateEntities;
import net.packet.server.UpdateHealth;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huch on 12.06.2016..
 */
public class Game {

    private Client player1, player2;
    private Client winner;

    /*
        TOP, RIGHT, BOTTOM, LEFT
     */
    private double[] boundaries = {0, 300, 200, 0};

    private Entity entity1;
    private Entity entity2;
    private List<Entity> entity1Bullets;
    private List<Entity> entity2Bullets;

    public Game(ConnectionListener connectionListener, Client player1, Client player2) {
        this.player1 = player1;
        this.player2 = player2;
        entity1 = new Entity(boundaries[1] / 2, boundaries[0] + 10);
        entity1.setHealth(100);
        entity2 = new Entity(boundaries[1] / 2, boundaries[2] - 30);
        entity2.setHealth(100);
        entity1Bullets = new ArrayList<Entity>();
        entity2Bullets = new ArrayList<Entity>();
        player1.getPacketSender().sendUpdateHealth(
                new UpdateHealth(entity1.getHealth(),
                        entity2.getHealth(),
                        player1.getPlayer().getUsername(),
                        player2.getPlayer().getUsername()),
                player1.getOutputStream());
        player1.getPacketSender().sendUpdateHealth(
                new UpdateHealth(entity1.getHealth(),
                        entity2.getHealth(),
                        player1.getPlayer().getUsername(),
                        player2.getPlayer().getUsername()),
                player2.getOutputStream());
        player1.setGame(this);
        player2.setGame(this);
        updateGame();
    }

    public void updateGame() {
        new Thread() {
            @Override
            public void run() {
                updateInput();
            }
        }.start();
    }

    private void updateInput() {
        player1.getPacketSender().sendEntities(new UpdateEntities(entity1, entity2, entity1Bullets, entity2Bullets), player1.getOutputStream());
        player2.getPacketSender().sendEntities(new UpdateEntities(entity1, entity2, entity1Bullets, entity2Bullets), player2.getOutputStream());
    }

    private void collision() {
    }

    public void setPosition1(String action) {
        System.out.println(action);
        if("A".equals(action)) {
            entity1.setX(entity1.getX() - 3);
        } else if("D".equals(action)) {
            entity1.setX(entity1.getX() + 3);
        }
    }

    public void setPosition2(String action) {
        System.out.println(action);
        if("A".equals(action)) {
           entity2.setX(entity2.getX() - 3);
        } else if("D".equals(action)) {
            entity2.setX(entity2.getX() + 3);
        }
    }

    public void setWinner(Client winner) {
        this.winner = winner;
    }

    public Client getWinner() {
        return winner;
    }

    public Client getPlayer() {
        return player1;
    }

    public Client getPlayer2() {
        return player2;
    }
}
