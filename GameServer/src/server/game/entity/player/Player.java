package server.game.entity.player;

import server.game.challenge.Challenge;

/**
 * Created by huch on 20.05.2016..
 */
public class Player {

    private Challenge challenge;

    private String username, password;

    public Player(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }

    public Challenge getChallenge() {
        return challenge;
    }

}
