package server.game.lobby;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huch on 11.06.2016..
 */
public class Lobby {

    private List<String> playersInLobby;

    public Lobby() {
        playersInLobby = new ArrayList<String>();
    }

    public void joinLobby(String username) {
        playersInLobby.add(username);
    }

    public boolean removeFromLobby(String username) {
        for(int i = 0; i < playersInLobby.size(); i++) {
            if(playersInLobby.get(i).equals(username)) {
                playersInLobby.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean inLobby(String username) {
        for(int i = 0; i < playersInLobby.size(); i++) {
            if(playersInLobby.get(i).equals(username)) {
                return true;
            }
        }
        return false;
    }

    public List<String> getPlayersInLobby() {
        return playersInLobby;
    }
}
