package server.game.challenge;

import net.Client;

/**
 * Created by huch on 12.06.2016..
 */
public class Challenge {

    private Client player1;
    private Client player2;

    private boolean player1Status = true;
    private boolean player2Status;

    private boolean ready = false;
    private boolean timerGone = false;

    public Challenge(Client player1, Client player2) {
        this.player1 = player1;
        this.player2 = player2;
        waitForPlayer2();
    }

    private void waitForPlayer2() {
        new Thread() {
            @Override
            public void run() {
                while(!player2Status)  {
                    //set timer
                    System.out.println(player1Status + " " + player2Status);
                }
                System.out.println(player1Status + " " + player2Status);

                if(player1Status && player2Status) {
                    ready = true;
                    timerGone = true;
                } else {

                    ready = false;
                    timerGone = true;
                }
            }

        }.start();
    }

    public Client getPlayer1() {
        return player1;
    }

    public Client getPlayer2() {
        return player2;
    }

    public void setPlayer1Status(boolean status) {
        this.player1Status = status;
    }

    public void setPlayer2Status(boolean status) {
        this.player2Status = status;
    }

    public boolean isReady() {
        return ready;
    }

    public boolean isTimerGone() {
        return timerGone;
    }

}
