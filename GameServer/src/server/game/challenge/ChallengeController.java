package server.game.challenge;

import net.Client;
import server.game.GameController;
import server.game.lobby.Lobby;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huch on 12.06.2016..
 */
public class ChallengeController {

    private Lobby lobby;

    private List<Challenge> challanges;

    private GameController gameController;

    public ChallengeController(Lobby lobby, GameController gameController) {
        challanges = new ArrayList<Challenge>();
        this.lobby = lobby;
        this.gameController = gameController;
        processChallanges();
    }

    public void addNewChallange(Client player1, Client player2) {
        Challenge ch = new Challenge(player1, player2);
        player1.getPlayer().setChallenge(ch);
        player2.getPlayer().setChallenge(ch);
        challanges.add(ch);
    }

    public synchronized void processChallanges() {
        new Thread() {
            @Override
            public void run() {
                while(true) {
                        for (Challenge ch : challanges) {
                            if (ch.isReady() && ch.isTimerGone()) {
                                //start game
                                System.out.println("Start game");
                                lobby.removeFromLobby(ch.getPlayer1().getPlayer().getUsername());
                                lobby.removeFromLobby(ch.getPlayer2().getPlayer().getUsername());
                                gameController.startNewGame(ch.getPlayer1(), ch.getPlayer2());
                                challanges.remove(ch);
                            } else if (!ch.isReady() && ch.isTimerGone()) {
                                System.out.println("Challenge ended");
                                ch.getPlayer1().getPlayer().setChallenge(null);
                                ch.getPlayer2().getPlayer().setChallenge(null);
                                challanges.remove(ch);
                            }
                        }

                }
            }
        }.start();
    }


}
