package server.game;

import net.Client;
import net.ConnectionListener;

/**
 * Created by huch on 12.06.2016..
 */
public class GameController {

   // private List<Game> games = new ArrayList<Game>();

    private ConnectionListener connectionListener;

    public GameController(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }

    public boolean startNewGame(Client player1, Client player2) {
        player1.getPacketSender().sendAnswer((byte)73, player1.getOutputStream());
        player2.getPacketSender().sendAnswer((byte)73, player2.getOutputStream());
        Game g = new Game(connectionListener, player1, player2);
        //send names
        //position players on table
       // games.add(g);
        //start game
        //do game things
        //stop game, put players back in lobby
        return true;
    }
}
