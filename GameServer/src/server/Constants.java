package server;

/**
 * Created by huch on 20.05.2016..
 */
public class Constants {

    /**
     * Server name.
     */
    public static final String SERVER_NAME = "testing purpose";

    /**
     * Server port.
     */
    public static final int SERVER_PORT = 1337;

    /**
     * Max users that can connect to server.
     */
    public static final int MAX_USERS = 10;
}
