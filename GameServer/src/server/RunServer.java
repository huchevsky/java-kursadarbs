package server;

import net.Server;

/**
 * Created by huch on 20.05.2016..
 * Main class, which starts new server.
 */
public class RunServer {

    public static void main(String args[]) {
        new Server("test", 1339);
    }
}
