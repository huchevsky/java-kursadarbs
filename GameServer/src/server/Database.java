package server;

import java.sql.*;

/**
 * Created by huch on 08.06.2016..
 */
public class Database {

    private Connection connection;
    private Statement stmn;

    public Database() {
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:gameserver.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean addUserToDatabase(String usr, String psw) {
        String sql = "INSERT INTO USERS (USERNAME, PASSWORD) " +
                "VALUES ('" + usr + "', '" + psw + "');";
        try {
            stmn = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        try {
            stmn.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean containsUser(String usr) {
        String sql = "SELECT USERNAME FROM USERS WHERE USERNAME='" + usr + "';";
        try {
            stmn = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        ResultSet rs = null;
        try {
            rs = stmn.executeQuery(sql);
            while ( rs.next() ) {
                String username = rs.getString("USERNAME");
                if(usr.equals(username)) {
                    return true;
                }
            }
            rs.close();
            stmn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean correctPassword(String usr, String password) {
        String sql = "SELECT PASSWORD FROM USERS WHERE USERNAME='" + usr + "';";
        try {
            stmn = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        ResultSet rs = null;
        try {
            rs = stmn.executeQuery(sql);
            while ( rs.next() ) {
                String psw = rs.getString("PASSWORD");
                if(password.equals(psw)) {
                    return true;
                }
            }
            rs.close();
            stmn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
