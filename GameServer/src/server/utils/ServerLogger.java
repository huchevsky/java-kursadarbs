package server.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by huch on 20.05.2016..
 * Custom logger for logging server events.
 */
public class ServerLogger {

    /**
     * Creates new Logger instance.
     */
    private final static Logger LOGGER = Logger.getLogger("Server");

    /**
     * Logs new info message.
     * @param clazz Class which is logging this message.
     * @param msg Log message.
     */
    public static void logInfo(Class clazz, String msg) {
        LOGGER.log(Level.INFO,"(" + clazz.getName() + ") " + msg);
    }

    /**
     * Logs new error message.
     * @param clazz Class which is logging this message.
     * @param msg Log message.
     */
    public static void logError(Class clazz, String msg) {
        LOGGER.log(Level.WARNING,"(" + clazz.getName() + ") " + msg);
    }

}
